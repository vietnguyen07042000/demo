abstract	Khai báo lớp, phương thức, interface trừu tượng không có thể hiện(instance) cụ thể
assert		Kiểm tra điều kiện đúng hay sai (thường dùng trong Unit Test)
boolean		Khai báo biến kiểu logic với 2 trị: true, false.
break		Thoát ra khỏi vòng lặp hoặc lệnh switch-case.
byte		Kiểu byte với các giá trị nguyên chiếm 8 bit (1 byte).
case		Trường hợp được tuyển chọn theo switch (chỉ được dùng khi đi kèm switch)
catch		Được sử dụng để bắt ngoại lệ, được sử dụng cùng với try để xử lý các ngoại lệ xảy ra trong chương trình
char		Kiểu ký tự Unicode, mỗi ký tự chiếm 16 bit (2 byte).
class		Được sử dụng để định nghĩa class
const		Chưa được sử dụng vì vậy bạn không thể dùng nó trong ngôn ngữ Java
continue	Dừng chu trình(iteration) lặp hiện tại và bắt đầu chu trình tiếp theo
default		Mặc định đươc thực thi khi không có case nào trả về giá trị true (dùng trong switch case)
do		Dùng trong vòng lặp do while
double		Kiểu số thực với các giá trị biểu diễn theo dạng dấu phẩy động 64 bit (8 byte).
else		Rẽ nhánh theo điều kiện ngược lại của if.
enum		Định nghĩa kiểu dữ liệu enum – gần giống với kiểu dữ liệu mảng nhưng các phần tử có thể bổ sung thêm các phương thức
extends		Được sử dụng để định nghĩa lớp con kế thừa các thuộc tính và phương thức từ lớp cha.
final	Chỉ ra các biến, phương thức không được thay đổi sau khi đã được định nghĩa. Các phương thức final không thể được kế thừa và override
finally	Thực hiện một khối lệnh đến cùng bất chấp các ngoại lệ có thể xảy ra. Được sử dụng trong try-catch
float	Kiểu số thực với các giá trị biểu diễn theo dạng dấu phẩy động 32 bit.
for	Sử dụng cho vòng lặp for với bước lặp được xác định trước
goto	Chưa được sử dụng
if	Lệnh chọn theo điều kiện logic
implements	Xây dựng một lớp mới cài đặt những phương thức từ interface xác định trước.
import	Yêu cầu một hay một số lớp ở các gói chỉ định cần nhập vào để sử dụng trong ứng dụng hiện thời.
instanceof	Kiểm tra xem một đối tượng nào đó có phải là một thể hiện của 1 class được định nghĩa trước hay không
int	Kiểu số nguyên với các giá trị chiếm 32 bit (4 byte).
interface	Được sử dụng để định nghĩa interface
long	Kiểu số nguyên lớn với các giá trị chiếm 64 bit (8 byte).
native	Giúp lập trình viên có thể sử dụng code được viết bằng các ngôn ngữ khác
new	Khởi tạo đối tượng
package	Xác định một gói sẽ chứa một số lớp ở trong file mã nguồn.
private	Khai báo biến dữ liệu, phương thức riêng trong từng lớp và chỉ cho phép truy cập trong lớp đó.
protected	Khai báo biến dữ liệu, phương thức chỉ được truy cập ở lớp cha và các lớp con của lớp đó.
public	Khai báo lớp, biến dữ liệu, phương thức công khai có thể truy cập ở mọi nơi trong hệ thống.
return	Kết thúc phương thức và trả về giá trị cho phương thức
short	Kiểu số nguyên ngắn với các giá trị chiếm 16 bit (2 byte).
static	Định nghĩa biến, phương thức của một lớp có thể được truy cập trực tiếp từ lớp mà không thông qua khởi tạo đôi tượng của lớp
super	Biến chỉ tới đối tượng ở lớp cha
switch	Sử dụng trong câu lệnh điều khiển switch case
synchronized	Chỉ ra là ở mỗi thời điểm chỉ có một đối tượng hoặc một lớp có thể truy nhập đến biến dữ liệu, hoặc phương thức loại đó, thường được sử dụng trong lập trình đa luồng (multithreading)
this	Biến chỉ tới đối tượng hiện thời.
throw	Tạo một đối tượng exception để chỉ định một trường hợp ngoại lệ xảy ra
throws	Chỉ định cho qua ngoại lệ khi exception xảy ra
transient	Chỉ định rằng nếu một đối tượng được serialized, giá trị của biến sẽ không cần được lưu trữ
try	Thử thực hiện cho đến khi gặp một ngoại lệ.
void	Chỉ định một phương thức không trả về giá trị
volatile	Báo cho chương trình dịch biết là biến khai báo volatile có thể thay đổi tùy ý trong các luồng (thread).
while	Được sử dụng trong lệnh điều khiển while